var express = require('express');
var router = express.Router();
var IndexSerializer = require('../serializers/home');

var userRoute = require('./users');
var otpAuthRoute = require('./authOtp');
var guestAuthRoute = require('./authGuest');
var calculationsRoute = require('./calculations');
var calculationEntitiesRoute = require('./calculationEntities');

router.get('/', function (req, res) {
  var text = {
    text: 'It works!'
  };

  var jsonapi = IndexSerializer.serialize(text);
  res.send(jsonapi);
});

router.use('/users', userRoute);
router.use('/auth/otp', otpAuthRoute);
router.use('/auth/guest', guestAuthRoute);
router.use('/calculations', calculationsRoute);
router.use('/calculation-entities', calculationEntitiesRoute);

module.exports = router;

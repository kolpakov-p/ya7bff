'use strict';
var express = require('express');
var router = express.Router();
var usersController = require('../controllers/users');
var authChecker = require('../plugins/authChecker');

router.use(authChecker);
router.get('/byToken', usersController.getProfile);
router.patch('/byToken', usersController.updateProfile);

module.exports = router;

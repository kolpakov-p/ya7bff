'use strict';
var express = require('express');
var router = express.Router();
var authOTPController = require('../controllers/authOtp');
var authChecker = require('../plugins/authChecker');

router.post('/request', authOTPController.sendOTP);
router.use(authChecker); // все запросы после этой вставки обрабатываются только при наличии токена
router.post('/verify', authOTPController.verifyOTP);

module.exports = router;

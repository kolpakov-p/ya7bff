'use strict';
var express = require('express');
var router = express.Router();
//var calculationsController = require('../controllers/calculations');
var calculationEntitiesController = require('../controllers/calculationEntities');
var authChecker = require('../plugins/authChecker');

router.use(authChecker); // все запросы обрабатываются только при наличии токена
router.post('/', calculationEntitiesController.create);
router.patch('/:id', calculationEntitiesController.update);
router.delete('/:id', calculationEntitiesController.delete);

//router.get('/', calculationEntitiesController.getAll); // нет необходимости пока что
//router.get('/:id', calculationEntitiesController.getOne); // нет необходимости пока что

module.exports = router;

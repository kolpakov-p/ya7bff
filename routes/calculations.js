'use strict';
var express = require('express');
var router = express.Router();
var calculationsController = require('../controllers/calculations');
var calculationEntitiesController = require('../controllers/calculationEntities');
var authChecker = require('../plugins/authChecker');

router.use(authChecker); // все запросы обрабатываются только при наличии токена
router.get('/', calculationsController.getAll); // получаем все калькуляции, доступные пользователю
router.get('/:id', calculationsController.getOne); // получаем одну калькуляцию, по ID (если доступна пользователю)
router.post('/', calculationsController.create); // создаем новую калькуляцию с привязкой к пользователю

router.get('/:calculationId/calculation-entities/', calculationEntitiesController.getAll); // получаем все сущности калькуляции

/*
router.get('/:calculationEntityId/relationships/calculations/', (req, res, next) => {
    req.params.noRelations = true; next()
}, calculationsController.getOne);
*/

module.exports = router;

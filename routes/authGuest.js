'use strict';
var express = require('express');
var router = express.Router();
var authGuestController = require('../controllers/authGuest');

router.get('/token', authGuestController.getToken);

module.exports = router;

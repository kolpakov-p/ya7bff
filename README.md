# ya7auto.ru JSON:API

API сервер, соответствующий спецификации [JSON API v1.0](https://jsonapi.org/)

## Настройки

Все настройки хранятся в файле config.js. Он разделен на две секции: настройка на время разработки и на время эксплуатации (боевые).

Предустановки меняются переменной окружения NODE_ENV.

Т.к. получени всех сущностей в отрыве от калькуляции лишено смысла, то API рассчитано на то, что получение сущностей калькуляции будет производиться одним из двух путей:
1. Через include и вложенные зависимости: /calculations/?include=calculation-entities
2. Через обращение по related link: /calculations/100000/calculation-entities 

## Запрос одноразового пароля

### Запрос

> POST /auth/otp/request/

```json
{
  "data": {
    "type": "otp-request",
    "attributes": {
      "credential": "7927..."
    }
  }
}
```

```json
{
  "data": {
    "type": "otp-request",
    "attributes": {
      "credential": "test@test.com"
    }
  }
}
```

### Ответ

Возращает статус 202, если пароль успешно отправлен; 400 если запрос неверный; 503 если отправить пароль не удалось


## Запрос гостевого токена

### Запрос

> POST /auth/guest/token/

```json
{
  "data": {
    "type": "?",
    "attributes": {
    }
  }
}
```
### Ответ



## Проверка пароля (запрос токена)

### Запрос

> POST /auth/otp/verify/

```json
{
  "data": {
    "type": "verify",
    "attributes": {
      "credential": "7927...",
      "password": "..."
    }
  }
}
```

### Ответ

Если пароль правильный, возращает статус 201 и данные был ли пользователь зарегистрирован ранее:

```json
{
  "data": {
    "id": "...",
    "type": "verify",
    "attributes": {
      "isAlreadyRegistered": false
    }
  }
}
```

Если пароль не верен или возникла ошибка, то об этом сообщается соответствующим HTTP кодом с расшифровкой в теле ответа.


## Калькуляции

### Получение данных

#### Запрос

> GET /calculations/

#### Ответ

```json
{
    "data": [
        {
            "type": "calculations",
            "id": "100000",
            "attributes": {
                "car-model-id": 1
            },
            "relationships": {
                "calculation-entities": {
                    "data": [
                        {
                            "type": "calculation-entities",
                            "id": "1"
                        }
                    ]
                }
            }
        },
        {
            "type": "calculations",
            "id": "100001",
            "attributes": {
                "car-model-id": 1
            }
        }
    ]
}
```

#### Запрос

> GET /calculations/100000

#### Ответ

```json
{
"data": {
  "type": "calculations",
  "id": "100000",
  "attributes": {
    "carModelId": 1
  },
  "relationships": {
    "calculationEntities": {
      "data": [
         {
            "type": "calculation-entities",
            "id": "1"
          }
        ]
      }
    }
  }
}
```

### Получение со связанными данными

#### Запрос

> GET /calculations/?include=calculation-entities

#### Ответ

```json
{
    "included": [
        {
            "type": "calculation-entities",
            "id": "1",
            "attributes": {
                "id": 1,
                "service-id": 1,
                "comment": "...",
                "data": {...},
                "price": 15000
            }
        }
    ],
    "data": [
      {
        "type": "calculations",
        "id": "100000",
        "attributes": {
            "car-model-id": 1
        },
        "relationships": {
            "calculation-entities": {
                "data": [
                    {
                        "type": "calculation-entities",
                        "id": "1"
                    }
                ]
            }
        }
      },
      ...
    ]
}
```

#### Запрос

> GET /calculations/100000/?include=calculation-entities

#### Ответ

```json
{
    "included": [
        {
            "type": "calculation-entities",
            "id": "1",
            "attributes": {
                "id": 1,
                "service-id": 1,
                "comment": "...",
                "data": {...},
                "price": 15000
            }
        }
    ],
    "data": {
        "type": "calculations",
        "id": "100000",
        "attributes": {
            "car-model-id": 1
        },
        "relationships": {
            "calculation-entities": {
                "data": [
                    {
                        "type": "calculation-entities",
                        "id": "1"
                    }
                ]
            }
        }
    }
}
```

## Сущности калькуляции

Получение всех сущностей (без указания ID калькуляции) недоступно (это не имеет смысла).

### Получение сущностей, привязанных с определенной калькуляции

#### Запрос

> GET /calculations/100000/calculation-entities

#### Ответ

```json
{
    "data": [
        {
            "type": "calculation-entities",
            "id": "1",
            "attributes": {
                "service_id": 1
            },
            "relationships": {
                "calculations": {
                    "data": [
                        {
                            "type": "calculations",
                            "id": "100000"
                        }
                    ]
                }
            }
        }
    ]
}
```

### Создание новой

Обязательно необходимо указать ID связанной калькуляции в объекте "relationships"

#### Запрос

> POST /calculation-entities/

```json
{
    "data": {
      "type": "calculation-entities",
      "attributes": {
        "service_id": 1
      },
      "relationships": {
        "calculations": {
          "data": {
            "type": "calculations",
            "id": "100000"
          }
        }
      }
    }
}
```

#### Ответ

```json
{
    "data": [
        {
            "type": "calculation-entities",
            "id": 3,
            "attributes": {
                "service_id": 1
            },
        }
    ]
}
```

### Обновление имеющейся

Смена принадлежности к калькуляциям не предусмотрена (не имеет смысла)

#### Запрос

> PATCH /calculation-entities/1

```json
{
    "data": [
        {
            "type": "calculation-entities",
            "id": 1,
            "attributes": {
                "service_id": 2,
                "comment": " ",
                "data": {

                },
                "price": 1000,
                "structure_version": "0.1"
            }
        }
    ]
}
```

#### Ответ

```json
{
    "data": [
        {
            "type": "calculation-entities",
            "id": 1,
            "attributes": {
                "service_id": 2,
                "comment": " ",
                "data": {

                },
                "price": 1000,
                "structure_version": "0.1"
            },
        }
    ]
}
```
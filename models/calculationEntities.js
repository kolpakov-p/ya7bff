var BaseModel = require('./index');

module.exports = class extends BaseModel {
    constructor () {
        super('calculation_entities');
    }

    async checkAffiliation(userId, entityId) {
        try {
            const [rows, wtf] = await this.connection.execute(
                "SELECT `id` FROM `"+this.tableName+"` WHERE `id` = ? AND `calculation_id` IN (SELECT `id` FROM `calculations` WHERE `user_id` = ?)",
                [entityId, userId]
            )

            return rows.length > 0 ? true : false;
        } catch (e) {
            console.log(e);
            return false;
        }
    }
}
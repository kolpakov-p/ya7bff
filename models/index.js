const mysql = require('mysql2');
var config = require('../config')

function stringifyObject (object) {
    let string = '';
    let setup = [];

    for (var key in object) {
        string += "`"+key+"` = ?, "
        setup.push(object[key])
    }

    string = string.slice(0, -2) // без последней запятой в конце

    return [string, setup]
}

function stringifyFields (object) {
    let string = ''
    let setup = []

    for (var key in object) {
        string += "`"+key+"` = ? AND "
        setup.push(object[key])
    }

    string = string.slice(0, -5) // без последней AND в конце

    return [string, setup]
}

module.exports = class {
    constructor(tableName) {
        this.connection = mysql.createPool(config.db).promise();
        this.tableName = tableName;
    }

    async getAllByFields(fields) {
        if (Array.isArray(fields)) return false; // не принимает массив как аргумент

        let [query, setup] = stringifyFields(fields);

        try {
            const [rows, f] = await this.connection.execute(
                "SELECT * FROM `"+this.tableName+"` WHERE "+query,
                setup
            )
    
            return rows.length > 0 ? rows : [];
        } catch (e) {
            console.log(e);
            return [];
        }
    }

    async getOneByFields(fields) {
        if (Array.isArray(fields)) return false; // не принимает массив как аргумент

        let [query, setup] = stringifyFields(fields);

        try {
            const [rows, f] = await this.connection.execute(
                "SELECT * FROM `"+this.tableName+"` WHERE "+query+" LIMIT 1",
                setup
            )
    
            return rows.length > 0 ? rows[0] : null;
        } catch (e) {
            console.log(e);
            return null;
        }
    }

    async insertOne(object) { // вставляет новую запись и возвращает ID новой записи или FALSE в случае неудачи
        if (Array.isArray(object)) return false; // не принимает массив как аргумент

        let [query, setup] = stringifyObject(object);

        try {
            const [result, wtf] = await this.connection.execute(
                "INSERT INTO `"+this.tableName+"` SET "+query,
                setup
            )

            return result.affectedRows > 0 ? result.insertId : null;
        } catch (e) {
            console.log(e);
            return null;
        }
    }

    async updateOne(id, object) {
        id = parseInt(id)
        if (id < 1 || Object.keys(object).length < 1) return false

        let [query, setup] = stringifyObject(object);

        try {
            const [result, wtf] = await this.connection.execute(
                "UPDATE `"+this.tableName+"` SET "+query+" WHERE `id` = ? LIMIT 1",
                [...setup, id]
            )

            return result.affectedRows > 0 ? true : false;
        } catch (e) {
            console.log(e);
            return false;
        }
    }

    async countByField(field, value) {
        const [rows, f] = await this.connection.execute(
            "SELECT COUNT(*) FROM `"+this.tableName+"` WHERE `"+field+"` = ?",
            [value]
        )

        return rows[0];
    }

    async deleteOne(id) {
        const [result, f] = await this.connection.execute(
            "DELETE FROM `"+this.tableName+"` WHERE `id` = ?",
            [id]
        )

        return result.affectedRows > 0 ? true : false;
    }
}

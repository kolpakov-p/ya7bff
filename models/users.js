var BaseModel = require('./index');

module.exports = class UsersModel extends BaseModel {
    constructor () {
        super('users');
    }
}
var config = require('./config')
var express = require('express');
//var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var cors = require('cors')

var routes = require('./routes/index');

var JSONAPIError = require('jsonapi-serializer').Error;

var app = express();

// uncomment after placing your favicon in /public
app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

var corsOptions = {
  origin: config.cors.origin,
  optionsSuccessStatus: 200
}
app.use(cors(corsOptions))

// Add headers
app.use(function (req, res, next) {
/*  res.setHeader('Access-Control-Allow-Origin', 'http://localhost');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
  res.setHeader('Access-Control-Allow-Credentials', true);*/
  res.setHeader("Content-Type", "application/vnd.api+json");

  next();
});


app.use('/', routes);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    var errors = {
      message: err.message,
      error: err
    };

    json = new JSONAPIError({
      code: err.status,
      source: {'pointer': req.url},
      title: err.message,
    });

    res.send(json);
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  var errors = {
    message: err.message,
    error: {}
  };

  json = new JSONAPIError({
    code: err.status,
    source: {'pointer': req.url},
    title: err.message,
  });

  res.send(json);
});


module.exports = app;
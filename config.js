const env = process.env.NODE_ENV || 'production';

const base = {
    app: { // backend port (this app)
        port: 3000
    },
    memcached: {
        address: 'localhost:11211',
        options: {
            failures: 3, timeout: 2000, retry: 2000, retries: 3
        }
    },
    jsonapi: {
        deserialization: {
            keyForAttribute: 'underscore_case'
        }
    }
}

const production = {
    cors: { // frontend url
        origin: 'https://ya7auto.ru'
    },
    db: {
        host: "mysql16.hostland.ru",
        user: "host1671060_modern",
        database: "host1671060_modern",
        password: "KvWn6B1v"
    }
}

const staging = {
    cors: { // frontend url
        origin: 'http://new.ya7auto.ru'
    },
    db: {
        host: "mysql16.hostland.ru",
        user: "host1671060_modern",
        database: "host1671060_modern",
        password: "KvWn6B1v"
    }
}

const development = {
    cors: { // frontend url
        origin: 'http://localhost'
    },
    db: {
        host: "mysql16.hostland.ru",
        user: "host1671060_modern",
        database: "host1671060_modern",
        password: "KvWn6B1v"
    }
}

const config = {
    development: {
        ...base,
        ...development
    },
    staging: {
        ...base,
        ...staging
    },
    production: {
        ...base,
        ...production
    }
}

module.exports = config[env];
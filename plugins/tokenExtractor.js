module.exports = function (req) {
  header = req.headers.authorization;
    
  regexpResult = new RegExp('^Bearer ([0-9A-Za-z]{64})$').exec(header);

  if (regexpResult !== null) {
    return regexpResult[1];
  } else {
    return null;
  }
}
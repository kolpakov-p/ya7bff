var APIError = require('../plugins/APIError');
var errorCatcher = require('../plugins/errorCatcher');
var UsersModel = new (require('../models/users'));
var tokenExtractor = require('../plugins/tokenExtractor');

module.exports = async function (req, res, next) {
  try {
    token = tokenExtractor(req);

    if (token === null) {
      throw new APIError(403, 'You must provide valid token');
    }

    user = await UsersModel.getOneByFields({ token });

    if (user === null) { // пользователь не найден по токену
      throw new APIError(403, 'You must provide valid token');
    }

    req.user = { id: user.id } // устанавливаем пользователя для дальнейшнего использования

    next();
  } catch (err) {
    errorCatcher(err, res);
  }
}
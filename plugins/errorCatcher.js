var APIError = require('./APIError');

module.exports = function (errObj, response) {
    if (errObj instanceof APIError) {
        errObj.send(response);
        return;
      } else {
        console.log(errObj);
        new APIError(500, errObj.message).send(response);
      }
}
var JSONAPIError = require('jsonapi-serializer').Error;

module.exports = class {
    constructor (code, message, name = '') {
        if (name == '') {
            switch (code) {
                case 500:
                    this.name = 'Internal Server Error'
                    break;
                case 400:
                    this.name = 'Bad Request'
                    break;
                case 429:
                    this.name = 'Too Many Requests'
                    break;
                case 503:
                    this.name = 'Service Unavailable'
                    break;
                case 406:
                    this.name = 'Not Acceptable'
                    break;
                case 403:
                    this.name = 'Forbidden'
                    break;
                case 404:
                    this.name = 'Not Found'
                    break;
            }
        } else {
            this.name = name;
        }
        
        this.message = message;
        this.code  = code;
    }

    send (response) {
        var json = new JSONAPIError({
            code: this.code,
            source: {'pointer': ''},
            title: this.name,
            detail: this.message
        });
          
        response.status(this.code);
        response.send(json);
    }
}
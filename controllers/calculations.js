var CalculationsModel = new (require('../models/calculations'));
var CalculationEntitiesModel = new (require('../models/calculationEntities'));
var config = require('../config')
var APIError = require('../plugins/APIError');
var errorCatcher = require('../plugins/errorCatcher');
var tokenExtractor = require('../plugins/tokenExtractor');
var JSONAPIDeserializer = require('jsonapi-serializer').Deserializer;

var CalculationsSerializer = require('../serializers/calculations');

exports.checkAffiliation = async function (userID=null, calculationID=null) {
  var calculation = await CalculationsModel.getOneByFields({ // получаем требуемую калькуляцию с проверкой принадлежиности пользователю
    'id': calculationID,
    'user_id': userID
  });

  if (calculation !== null) { // если калькуляция нашлась
    return true
  } else { // если не нашлась
    return false
  }
}

/**
 * Получаем все калькуляции аутентифицированного пользователя
 */
exports.getAll = async function(req, res) {
  try {
    var jsonResp // здесь храним HTTP ответ
    //var token = tokenExtractor(req); // на вшивость токен можно не проверять, авторизация уже пройдена
    var userId = req.user.id // ID пользователя из параметров маршрута (установлено ранее, при проверке токена)
    //var noRelations = req.params.noRelations; // параметр: если не нужно включать зависимости
    var include = req.query.include // GET параметр, указывающий, нужно ли включить данные зависимостей

    var calculations = []

    var calculations = await CalculationsModel.getAllByFields({ 'user_id': userId }); // выбираем все калькуляции

    //if (!noRelations) { // если зависимости нужны
      for (var key in calculations) { // перебираем по одной калькуляции
        var relations = await CalculationEntitiesModel.getAllByFields({ // выбираем зависимости по ID калькуляции
          'calculation_id': calculations[key].id,
        });

        if (relations.length > 0) { // если зависимости есть
          calculations[key]['calculation-entities'] = relations // устанавливаем в объект калькуляции
        }
      }
    //}

    // готовим ответ
    //if (noRelations) {
    //  jsonResp = CalculationsSerializer.withoutRelationships.serialize(calculations); // если зависимости не нужны
    //} else {
      if (include !== undefined && include != '') { // если запрос на включение зависимостей не пуст
        if (include == 'calculation-entities') { // если запросили сущности
          jsonResp = CalculationsSerializer.withIncludes.serialize(calculations);
          
        } else { // если запросили то, чего мы не знаем
          throw new APIError(400, 'Unexpected includes requirement');
        }
      } else { // если зависимости не запрашивали
        jsonResp = CalculationsSerializer.withoutIncludes.serialize(calculations); // если нужны зависимости, но не нужны данные
      }
    //}
    
    res.send(jsonResp); // отправили ответ

  } catch (err) {
    errorCatcher(err, res)
  }
}

/**
 * Получаем указанную калькуляцию со вложенными данными
 */
exports.getOne = async function(req, res) {
  try {
    var jsonResp // здесь храним HTTP ответ
    //var token = tokenExtractor(req); // на вшивость токен можно не проверять, авторизация уже пройдена
    var userId = req.user.id // ID пользователя из параметров маршрута (установлено ранее, при проверке токена)
    var id = req.params.id // получаем ID калькуляции из маршрута
    //var noRelations = req.params.noRelations
    var include = req.query.include // GET параметр, указывающий, данные каких зависимостей необходимо включить в ответ

    var calculation = {}
    var relations = {}

    var calculation = await CalculationsModel.getOneByFields({ // получаем требуемую калькуляцию с проверкой принадлежиности пользователю
      id,
      'user_id': userId
    });

    if (calculation !== null) { // если калькуляция нашлась
      //if (!noRelations) {
        var relations = await CalculationEntitiesModel.getAllByFields({ // выбираем зависимости по ID калькуляции
          'calculation_id': calculation.id,
        });
  
        if (relations.length > 0) { // если зависимости есть
          calculation['calculation-entities'] = relations // устанавливаем в объект калькуляции
        }
      //}
    } else { // если не нашлась
      throw new APIError(404, 'No user\'s calculation with this ID');
    }

    //if (noRelations) {
    //  jsonResp = CalculationsSerializer.withoutRelations.serialize(calculation);
    //} else {
      if (include !== undefined && include != '') { // если запрос на включение зависимостей не пуст
        if (include == 'calculation-entities') { // если запросили сущности
          jsonResp = CalculationsSerializer.withIncludes.serialize(calculation);
          
        } else { // если запросили то, чего мы не знаем
          throw new APIError(400, 'Unexpected includes requirement');
        }
      } else { // если зависимости не запрашивали
        jsonResp = CalculationsSerializer.withoutIncludes.serialize(calculation); // если нужны зависимости, но не нужны данные
      }
    //}

    res.send(jsonResp);

  } catch (err) {
    errorCatcher(err, res)
  }
}

/**
 * Создаем новую с привязкой к пользователю
 */
exports.create = async function(req, res) {
  try {
    var jsonResp // здесь храним HTTP ответ
    var userId = req.user.id // ID пользователя из параметров маршрута (установлено ранее, при проверке токена)
    var carModelId
    
    deserializer = new JSONAPIDeserializer(config.jsonapi.deserialization);
    reqData = await deserializer.deserialize(req.body);

    if (reqData.carModelId) { // если получили информацию об автомобиле
      carModelId = reqData.carModelId
    } else {
      carModelId = null
    }

    var newCalculationID = await CalculationsModel.insertOne({ // вставляем в базу
      'user_id': userId,
      'car_model_id': carModelId
    }) 
    
    if (newCalculationID !== null) {
      var newCalculation = await CalculationsModel.getOneByFields({ // получаем новую калькуляцию
        id: newCalculationID
      })
    }

    jsonResp = CalculationsSerializer.withoutIncludes.serialize(newCalculation) // отдаем браузеру информацию о новой калькуляции
    res.send(jsonResp);

  } catch (err) {
    errorCatcher(err, res)
  }
}


var UsersModel = new (require('../models/users'));
var config = require('../config')
var APIError = require('../plugins/APIError');
var errorCatcher = require('../plugins/errorCatcher');
var tokenExtractor = require('../plugins/tokenExtractor');
var JSONAPIDeserializer = require('jsonapi-serializer').Deserializer;
var CalculationsController = require('../controllers/calculations');

var UsersSerializer = require('../serializers/users');

/**
 * Получаем профиль пользователя по токену
 */
exports.getProfile = async function(req, res) {
  try {
    token = tokenExtractor(req); // на вшивость токен можно не проверять, авторизация уже пройдена
    user = await UsersModel.getOneByFields({ token });

    if (user !== null) { // пользователь найден по токену
      jsonResp = UsersSerializer.serialize(user);
    } else { 
      throw new APIError(403, 'You must provide valid token');
    }
    
    res.send(jsonResp);
  } catch (err) {
    errorCatcher(err, res)
  }
};

/**
 * Обновление профиля пользователя
 */
exports.updateProfile = async function(req, res) {
  try {
    var userId = req.user.id // ID пользователя из параметров маршрута

    deserializer = new JSONAPIDeserializer(config.jsonapi.deserialization);
    reqData = await deserializer.deserialize(req.body); // добавить проверку на отсутствие массива

    for (var key in reqData) {
      if (key != 'last_calculation_id' && key != 'name' && key != 'email' && key != 'is_guest' && key != 'phone') {
        delete reqData[key]
      }
    }

    if (reqData['last_calculation_id'] !== undefined) {
      if (! CalculationsController.checkAffiliation(userId, reqData['last_calculation_id']) ) { // проверяем принадлежность калькуляции текущему пользователю
        throw new APIError(404, 'No user\'s calculation with this ID');
      }
    }

    updareResult = await UsersModel.updateOne(userId, reqData);

    if (updareResult) {
      res.status(204)
      res.send();
    } else {
      throw new APIError(500, 'Can\'t update user\'s information');
    }
  } catch (err) {
    errorCatcher(err, res)
  }
}


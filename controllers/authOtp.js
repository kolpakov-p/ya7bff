var Users = new (require('../models/users'));
var CalculationsModel = new (require('../models/calculations'));
var config = require('../config')
var APIError = require('../plugins/APIError');
var errorCatcher = require('../plugins/errorCatcher');

var JSONAPIDeserializer = require('jsonapi-serializer').Deserializer;
var VerifySerializer = require('../serializers/verify');
var tokenExtractor = require('../plugins/tokenExtractor');

var Memcached = require('memcached-promise');
var memcached = new Memcached(config.memcached.address, config.memcached.options);

var phoneValidator = new RegExp('^79[0-9]{9}$');
var emailValidator = new RegExp('^.*@.*\..*$');

// Отправка пароля по SMS
exports.sendOTP = async function(req, res) {
  try {
    deserializer = new JSONAPIDeserializer(config.jsonapi.deserialization);
    reqData = await deserializer.deserialize(req.body);
    var credential = {
      type: null,
      value: null
    };

    if (Array.isArray(reqData)) { // если прислали массив, вместо одного объекта
      throw new APIError(400, 'This endpoint can apply only one request at time');
    }

    // ищем в запросе реквизиты для аутентификациии
    if (reqData.credential !== undefined) {
      if (emailValidator.test(reqData.credential)) {
        credential.type = 'email';
      }
      if (phoneValidator.test(reqData.credential)) {
        credential.type = 'phone';
      }
    }
    
    if (credential.type === null) {
      throw new APIError(400, 'Credentials not found or have bad format');
    }

    credential.value = reqData.credential;

    var otp = Math.floor(
      Math.random() * (9999-1000) + 1000
    )
    
    // добавить логирование в dev
    console.log(otp); 
          
    if (credential.type === 'phone') { // аутентифицируемся по SMS
      oldPassword = await memcached.get(credential.value)  // ищем запись в хранилище

      if (oldPassword !== undefined) { // если в хранилище не пусто и аутентифицируемся по SMS
        throw new APIError(429, 'You can send only one message in 120 seconds');
      }

      // отправляем SMS
      var SMSru = require('sms_ru');
      var sms = new SMSru('A05792D5-10E9-068C-AFB9-34CC17575436');

      sms.sms_send({
        to: credential.value,
        text: 'Одноразовый пароль: '+otp,
        test: true
        }, async function (result) {
          try {
            if (result.code != '100') {
              console.log(result)
              throw new APIError(503, 'Can\'t send message via SMS');
            } else {
              var memSet = await memcached.set(credential.value, otp, 120); // записываем временный пароль в хранилище
              
              if (memSet === true) {
                res.status(204);
                res.send();
              } else {
                throw new APIError(503, 'OTP storage server error');
              }
            }

          } catch (err) {
            errorCatcher(err, res)
          }
        });
    } else if (credential.type === 'email') {
      sendEmail();
    } 

  } catch (err) {
    errorCatcher(err, res)
  }
};

exports.verifyOTP = async function(req, res) {
  try {
    var guestToken = tokenExtractor(req) // текущий токен гостя
    var guestUser = await Users.getOneByFields({ token: guestToken }); // текущий пользователь-гость

    if (guestUser === null) {
      throw new APIError(503, 'Can\'t fetch guest user token');  
    }

    //var randtoken = require('rand-token');
    // get otp from parameters
    deserializer = new JSONAPIDeserializer(config.jsonapi.deserialization);
    reqData = await deserializer.deserialize(req.body);
    var credential = {
      type: null,
      value: null
    };

    if (Array.isArray(reqData)) { // если прислали массив, вместо одного объекта
      throw new APIError(400, 'This endpoint can apply only one request at time');
    }

    if (reqData.id) { // если прислали сгенерированный ID
      throw new APIError(403, 'Can\'t create token with your ID');
    }

    // ищем в запросе реквизиты для аутентификациии
    if (reqData.credential !== undefined) {
      if (emailValidator.test(reqData.credential)) {
        credential.type = 'email';
      }
      if (phoneValidator.test(reqData.credential)) {
        credential.type = 'phone';
      }
    }

    if (credential.type === null) {
      throw new APIError(400, 'Credentials not found or have bad format');
    }

    credential.value = reqData.credential;

    var password = parseInt(reqData.password);

    memData = await memcached.get(credential.value); // ищем запись в хранилище

    if (memData === undefined) {
      throw new APIError(406, 'No password records with this credentials');
    }

    if (parseInt(memData) === password) { // если пароль совпал
      //token = randtoken.generate(64); // генерируем токен
      var alreadyRegistered = false

      var fields = {}
      fields[credential.type] = credential.value

      user = await Users.getOneByFields(fields); // ищем пользователя по телефону или почте

      // если пользователь с такими данными уже есть
      if (user !== null) { 
        alreadyRegistered = true

        // переносим расчет на имеющегося пользователя
        let transferResult = await CalculationsModel.updateOne( // переносим имеющиеся расчеты на зарегистрированного пользователя
          guestUser['last_calculation_id'], // последнюю калькуляцию гостевого пользователя
          { 'user_id': user.id } // переписываем на зарегистрированного пользователя
        )

        if (transferResult) {
          let deleteResult = await Users.deleteOne(guestUser.id) // удаляем гостевого пользователя

          if (deleteResult) {

            let updateResult = await Users.updateOne(user.id, {
              'token': guestToken, // токен от гостевого пользователя
              'last_calculation_id': guestUser['last_calculation_id'] // калькуляция гостевого пользователя
            })

            if (!updateResult) {
              throw new APIError(503, 'Can\'t set authenticated user token');  
            }
          } else {
            throw new APIError(503, 'Can\'t delete guest user');
          }
        } else {
          throw new APIError(503, 'Can\'t transfer calculations to authenticated user');
        }

      // если пользователь не был зарегистрирован на постоянно
      } else { 
        alreadyRegistered = false

        var updateData = {
          'is_guest': 0, // установили флаг постоянного пользователя
        }
        updateData[credential.type] = credential.value // установили реквизит для аутентификации

        updateResult = await Users.updateOne(guestUser.id, updateData);

        if (!updateResult) {
          throw new APIError(503, 'Can\'t update user information');
        }
      }

      var respJson = VerifySerializer.serialize({
        id: guestToken,
        isAlreadyRegistered: alreadyRegistered
      })
    } else {
      throw new APIError(403, 'Check your password');
    }

    res.status(201);
    res.send(respJson);
  } catch (err) {
    errorCatcher(err, res)
  }
}
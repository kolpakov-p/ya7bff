var CalculationsController = require('../controllers/calculations');
var CalculationEntitiesModel = new (require('../models/calculationEntities'));
var config = require('../config')
var APIError = require('../plugins/APIError');
var errorCatcher = require('../plugins/errorCatcher');
var JSONAPIDeserializer = require('jsonapi-serializer').Deserializer;

var CalculationEntitiesSerializer = require('../serializers/calculationEntities');
const calculationEntities = require('../serializers/calculationEntities');

/**
 * Получаем все сущности
 */
exports.getAll = async function(req, res) {
  try {
    var calculationId = req.params.calculationId // ID калькуляции, сущности которой мы получаем
    var userId = req.user.id // ID пользователя (установлено при проверке аутентификации)
    var jsonResp // тут храним HTTP ответ
    var include = req.query.include // GET параметр, указывающий, данные каких зависимостей необходимо включить в ответ

    if (include !== undefined && include != '') {
      throw new APIError(400, 'Unexpected includes'); // возвращаем ошибку на любой запрос данных зависимостей
    }

    var entities = []

    if (! CalculationsController.checkAffiliation(userId, calculationId) ) { // проверяем принадлежность калькуляции текущему пользователю
      throw new APIError(404, 'No user\'s calculation with this ID');
    }

    var entities = await CalculationEntitiesModel.getAllByFields({ 'calculation_id': calculationId }); // выбираем все калькуляции

    jsonResp = CalculationEntitiesSerializer.serialize(entities); // если нужны зависимости, но не нужны данные

    res.send(jsonResp); // отправили ответ

  } catch (err) {
    errorCatcher(err, res)
  }
}

/**
 * Получаем указанную сущность по ID
 */
// exports.getOne = async function(req, res) {
//   try {
//     var token = tokenExtractor(req); // на вшивость токен можно не проверять, авторизация уже пройдена

//     var id = req.params.id
//     var calculationId = req.params.calculationId
//     var noRelations = req.params.noRelations
//     var include = req.query.include
//     var userId = req.user.id
//     var jsonResp

//     var entity = {}

//     var calculations = await CalculationsModel.getAllByFields({ 'user_id': userId }); // выбираем все калькуляции пользователя

//     // проверяем наличие калькуляции с этим ID (из доступных пользователю)
//     searchResult = calculations.find((element, index) => {
//       return element.id == id ? true : false
//     })

//     if (searchResult == false) {
//       throw new APIError(403, 'You can query only your entities');
//     }

//     var entity = await CalculationEntitiesModel.getOneByFields({ id });

//     if (!noRelations) {
//       var relations = await CalculationsModel.getOneByFields({
//         'id': entity.calculation_id,
//       });

//       if (relations.length > 0) {
//         entity['calculations'] = relations
//       }
//     }

//     if (noRelations) {
//       jsonResp = CalculationEntitiesSerializer.withoutRelations.serialize(entity);
//     } else {
//       if (include !== undefined && include != '') {
//         if (include == 'calculations') {
//           jsonResp = CalculationEntitiesSerializer.withIncludes.serialize(entity);
          
//         } else {
//           throw new APIError(400, 'Unexpected includes');
//         }
//       } else {
//         jsonResp = CalculationEntitiesSerializer.withoutIncludes.serialize(entity);
//       }
//     }

//     res.send(jsonResp);

//   } catch (err) {
//     errorCatcher(err, res)
//   }
// }


/**
 * Создаем новую сущность
 */
exports.create = async function(req, res) {
  try {
    var jsonResp // здесь храним HTTP ответ
    var userId = req.user.id // ID пользователя из параметров маршрута (установлено ранее, при проверке токена)

    var deserializer = new JSONAPIDeserializer({
      ...config.jsonapi.deserialization, // объединяем с настройками из конфига
      'calculations': { // получаем связь с калькуляцией из данных
        valueForRelationship: function (relationship) {
          return relationship.id
        }
      }
    })
    reqData = await deserializer.deserialize(req.body)

    reqData.calculation_id = reqData.calculations

    if (Array.isArray(reqData) || Array.isArray(reqData.calculation_id)) { // проверка не прислали ли нам массив
      throw new APIError(409, 'Unexpected data provided. You must provide only one object, not array');
    }

    let calculationAffiliated = await CalculationsController.checkAffiliation(userId, reqData.calculation_id) // проверяем принадлежность калькуляции текущему пользователю

    if ( !calculationAffiliated ) { 
      throw new APIError(404, 'No user\'s calculation with this ID')
    }

    for (var key in reqData) { // очищаем данные (оставляем только существующие поля)
      if (key != 'service_id' && key != 'data' && key != 'comment' && key != 'price' && key != 'structure_version' && key != 'calculation_id') {
        delete reqData[key]
      }
    }

    if (reqData.service_id === undefined) reqData.service_id = null // на всякий случай, проверяем ID услуги

    var newEntityID = await CalculationEntitiesModel.insertOne(reqData)

    if (newEntityID === null) throw new APIError(500, 'Can\'t save new entity')

    var newEntity = await CalculationEntitiesModel.getOneByFields({ // получаем новую калькуляцию
      id: newEntityID
    })

    newEntity.calculations = { id: newEntity.calculation_id } // добавляем информацию о связи с калькуляцией

    jsonResp = CalculationEntitiesSerializer.serialize(newEntity)
    res.send(jsonResp)

  } catch (err) {
    errorCatcher(err, res)
  }
}

/**
 * Обновляем сущность
 */
exports.update = async function(req, res) {
  try {
    var jsonResp // здесь храним HTTP ответ
    var userId = req.user.id // ID пользователя из параметров маршрута (установлено ранее, при проверке токена)
    var id = req.params.id // получаем ID сущности из маршрута

    deserializer = new JSONAPIDeserializer(config.jsonapi.deserialization);
    reqData = await deserializer.deserialize(req.body); // добавить проверку на отсутствие массива

    for (var key in reqData) {
      if (key != 'service_id' && key != 'comment' && key != 'price' && key != 'structure_version' && key != 'data') {
        delete reqData[key]
      }
    }

    if (typeof reqData.data !== 'string') { // проверяем принадлежность этой сущности пользователю
      throw new APIError(400, 'The `data` must be a STRING, not object')
    }

    if (! CalculationEntitiesModel.checkAffiliation(userId, id) ) { // проверяем принадлежность этой сущности пользователю
      throw new APIError(404, 'No user\'s calculation entity with this ID')
    }

    updareResult = await CalculationEntitiesModel.updateOne(id, reqData);

    if (updareResult) {
      res.status(204)
      res.send()
    } else {
      throw new APIError(500, 'Can\'t update entity information');
    }
  } catch (err) {
    errorCatcher(err, res)
  }
}

/**
 * Удаляем сущность
 */
exports.delete = async function(req, res) {
  try {
    var userId = req.user.id // ID пользователя из параметров маршрута (установлено ранее, при проверке токена)
    var id = req.params.id // получаем ID сущности из маршрута

    if (! CalculationEntitiesModel.checkAffiliation(userId, id) ) { // проверяем принадлежность этой сущности пользователю
      throw new APIError(404, 'No user\'s calculation entity with this ID')
    }

    deleteResult = await CalculationEntitiesModel.deleteOne(id);

    if (deleteResult) {
      res.status(204)
      res.send()
    } else {
      throw new APIError(500, 'Entity delete failed');
    }
  } catch (err) {
    errorCatcher(err, res)
  }
}
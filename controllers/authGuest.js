var Users = new (require('../models/users'));

var APIError = require('../plugins/APIError');
var errorCatcher = require('../plugins/errorCatcher');

var TokensSerializer = require('../serializers/tokens');

/** 
 * Генерируем и выдаем токен пользователю
 */
exports.getToken = async function(req, res) {
  try {
    var randtoken = require('rand-token');
    
    var token = randtoken.generate(64);

    var respJson = TokensSerializer.serialize({
      id: token,
      value: token,
    });

    let userData = {
      'token': token,
      'is_guest': 1
    };
    
    let insertResult = await Users.insertOne(userData);

    if (!insertResult) {
      throw new APIError(503, 'Can\'t add new user');
    }

    res.status(201);
    res.send(respJson);
  } catch (err) {
    errorCatcher(err, res)
  }
}
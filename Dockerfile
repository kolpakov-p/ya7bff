ARG deploy_env
FROM node:10-alpine
ENV APP_ROOT /api
ENV NODE_ENV=$deploy_env

WORKDIR ${APP_ROOT}
ADD . ${APP_ROOT}

RUN npm ci

CMD ["npm", "run", "start"]
'use strict';
var JSONAPISerializer = require('jsonapi-serializer').Serializer;

// exports.withIncludes = new JSONAPISerializer('calculation-entities', {
//   attributes: ['service_id', 'comment', 'data', 'price'],
//   calculations: {
//     ref: 'id',
//     attributes: ['car_model_id'],
//     included: true,
//   },
//   keyForAttribute: 'camelCase'
// });

// exports.withoutIncludes = new JSONAPISerializer('calculation-entities', {
//   attributes: ['service_id', 'comment', 'data', 'price'],
//   calculations: {
//     ref: 'id',
//     included: false,
//   },
//   keyForAttribute: 'camelCase'
// });

module.exports = new JSONAPISerializer('calculation-entities', {
  attributes: ['service_id', 'comment', 'data', 'price', 'structure_version', 'calculations'],
  calculations: {
      ref: 'id',
      included: false,
  },
  keyForAttribute: 'camelCase'
});
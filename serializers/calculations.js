'use strict';
var JSONAPISerializer = require('jsonapi-serializer').Serializer;

exports.withIncludes = new JSONAPISerializer('calculations', {
  attributes: ['car_model_id', 'calculation-entities'],
  'calculation-entities': {
    ref: 'id',
    attributes: ['id', 'service_id', 'comment', 'data', 'price', 'structure_version'],
    included: true,
  },
  keyForAttribute: 'camelCase',
});

exports.withoutIncludes = new JSONAPISerializer('calculations', {
  attributes: ['car_model_id', 'calculation-entities'],
  'calculation-entities': {
    ref: 'id',
    included: false,
  },
  keyForAttribute: 'camelCase',
});

exports.withoutRelations = new JSONAPISerializer('calculations', {
  attributes: ['car_model_id'],
  keyForAttribute: 'camelCase',
});
'use strict';
var JSONAPISerializer = require('jsonapi-serializer').Serializer;

module.exports = new JSONAPISerializer('tokens', {
  attributes: ['value'],
  keyForAttribute: 'camelCase'
});

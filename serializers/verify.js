'use strict';
var JSONAPISerializer = require('jsonapi-serializer').Serializer;

module.exports = new JSONAPISerializer('verify', {
  attributes: ['isAlreadyRegistered'],
  keyForAttribute: 'camelCase'
});

'use strict';
var JSONAPISerializer = require('jsonapi-serializer').Serializer;

module.exports = new JSONAPISerializer('users', {
  attributes: ['id', 'name', 'phone', 'email', 'is_guest', 'last_calculation_id'],
  keyForAttribute: 'camelCase'
});
